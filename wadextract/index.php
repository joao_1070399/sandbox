<style>
	table, th, td{
		border: 1px solid black;
	}
	th, td{
		padding: 4px;
		width: 128px;
	}
</style>

<?php
	// PRETTY PRINT
	function pr($v){
		echo "<pre>";
			print_r($v);
		echo "</pre>";
	}

	// FEED IT A HEX STRING, AND WHICH BYTES YOU WANT TO CONVERT, AND IT WILL ORDER THEM BY LITTLE-ENDIAN ORDER AND CONVERT THEM TO AN DECIMAL INT
	function getInt($start, $end, $content){
		$start = $start * 2;
		$end = $end * 2;
		$length = $end - $start;
		$temp = substr($content, $start, $length);
		$final = "";
		for($i = (strlen($temp) - 1); $i >= 0; $i = $i - 2){
			$final .= $temp[$i - 1].$temp[$i];
		}
		return base_convert($final, 16, 10);
	}

	// FEED IT A HEX STRING, AND WHICH BYTES YOU WANT TO CONVERT, AND IT WILL CONVERT THEM TO A STRING
	function getString($start, $end, $content){
		$length = $end - $start;
		$final = substr($content, $start, $length);
		return $final;
	}

	// FEED IT AN ASCII STRING, AND WHICH BYTES YOU WANT TO CONVERT, AND IT WILL CONVERT THEM TO A HEX STRING
	function getHex($start, $end, $content){
		$start = $start * 2;
		$end = $end * 2;
		$length = $end - $start;
		$temp = substr($content, $start, $length);
		$final = "";
		for($i = (strlen($temp) - 1); $i >= 0; $i = $i - 2){
			$final .= $temp[$i - 1].$temp[$i];
		}
		return $final;
	}

	function getHexBE($start, $end, $content){
		$start = $start * 2;
		$end = $end * 2;
		$length = $end - $start;
		$final = substr($content, $start, $length);
		return $final;
	}

	// THIS IS THE HEX STRING WITH EVERY COLOR IN THE DOOM PALETTE
	// IT'S ORGANIZED IN THE FOLLOWING STRUCTURE: RR,GG,BB,00
	$paletteString = "000000001F170B00170F07004B4B4B00FFFFFF001B1B1B00131313000B0B0B00070707002F371F00232B0F00171F07000F1700004F3B2B00473323003F2B1B00FFB7B700F7ABAB00F3A3A300EB979700E78F8F00DF878700DB7B7B00D3737300CB6B6B00C7636300BF5B5B00BB575700B34F4F00AF474700A73F3F00A33B3B009B333300972F2F008F2B2B008B232300831F1F007F1B1B0077171700731313006B0F0F00670B0B005F0707005B070700530707004F0000004700000043000000FFEBDF00FFE3D300FFDBC700FFD3BB00FFCFB300FFC7A700FFBF9B00FFBB9300FFB38300F7AB7B00EFA37300E79B6B00DF936300D78B5B00CF835300CB7F4F00BF7B4B00B3734700AB6F4300A36B3F009B633B008F5F3700875733007F532F00774F2B006B4727005F432300533F1F004B371B003F2F1700332B13002B230F00EFEFEF00E7E7E700DFDFDF00DBDBDB00D3D3D300CBCBCB00C7C7C700BFBFBF00B7B7B700B3B3B300ABABAB00A7A7A7009F9F9F0097979700939393008B8B8B00838383007F7F7F00777777006F6F6F006B6B6B00636363005B5B5B00575757004F4F4F0047474700434343003B3B3B00373737002F2F2F00272727002323230077FF6F006FEF670067DF5F005FCF57005BBF4F0053AF47004B9F3F00439337003F832F0037732B002F63230027531B001F43170017330F0013230B000B170700BFA78F00B79F8700AF977F00A78F77009F876F009B7F6B00937B63008B735B00836B57007B634F00775F4B006F57430067533F005F4B370057433300533F2F009F8363008F775300836B4B00775F3F00675333005B472B004F3B230043331B007B7F63006F735700676B4F005B63470053573B00474F33003F472B00373F2700FFFF7300EBDB5700D7BB4300C39B2F00AF7B1F009B5B130087430700732B0000FFFFFF00FFDBDB00FFBBBB00FF9B9B00FF7B7B00FF5F5F00FF3F3F00FF1F1F00FF000000EF000000E3000000D7000000CB000000BF000000B3000000A70000009B0000008B0000007F00000073000000670000005B0000004F00000043000000E7E7FF00C7C7FF00ABABFF008F8FFF007373FF005353FF003737FF001B1BFF000000FF000000E3000000CB000000B30000009B000000830000006B0000005300FFFFFF00FFEBDB00FFD7BB00FFC79B00FFB37B00FFA35B00FF8F3B00FF7F1B00F3731700EB6F0F00DF670F00D75F0B00CB570700C34F0000B7470000AF430000FFFFFF00FFFFD700FFFFB300FFFF8F00FFFF6B00FFFF4700FFFF2300FFFF0000A73F00009F370000932F0000872300004F3B2700432F1B00372313002F1B0B00000053000000470000003B0000002F00000023000000170000000B0000000000FF9F4300FFE74B00FF7BFF00FF00FF00CF00CF009F009B006F006B00A76B6B";

	$paletteArray = array();
	$colorArray = array();

	// WE'RE PLACING IT ON AN ASSOCIATIVE ARRAY FIRST - AS TEXT
	for($i = 0; $i < (strlen($paletteString) / 8); $i++){
		$paletteArray[$i]["r"] = hexdec($paletteString[($i * 8)].$paletteString[($i * 8) + 1]);
		$paletteArray[$i]["g"] = hexdec($paletteString[($i * 8) + 2].$paletteString[($i * 8) + 3]);
		$paletteArray[$i]["b"] = hexdec($paletteString[($i * 8) + 4].$paletteString[($i * 8) + 5]);
	}

	// THIS IS WHERE THE APPLICATION STARTS
	// THIS WILL TRY AND GET EVERY LUMP FROM THE WAD
	// THIS GETS THE CONTENTS IN ASCII AND HEXADECIMAL
	// TESTED USING http://localhost/sandbox/WADExtract/index.php?wad=q1tex.wad
	$initWad = $_GET["wad"];
	$content = file_get_contents($initWad);
	$final = bin2hex($content);

	// THIS GETS THE NUMBER OF LUMPS, AND THE POINTER TO WHERE IT STARTS
	$size = getInt(4, 8, $final);
	$mem = getInt(8, 12, $final);
	// THIS CREATES AN ARRAY OF LUMPS
	$lumps = array();
	// DUE TO THE STRUCTURE OF THE DIRECTORY (4 BYTES = POINTER, 4 BYTES = SIZE, 8 BYTES = NAME), WE CAN ASSIGN THE VALUE OF EACH LUMP AUTOMATICALLY
	for($i = 0; $i < $size; $i++){
		$lumps[$i]["pointer"] = getInt(($mem + ($i * 16)), ($mem + ($i * 16)) + 4, $final);
		$lumps[$i]["size"] = getInt(($mem + ($i * 16)) + 4, ($mem + ($i * 16)) + 8, $final);
		$lumps[$i]["name"] = getString(($mem + ($i * 16)) + 8, ($mem + ($i * 16)) + 16, $content);
	}

	$cat = "Skip";
	for($i = 0; $i < count($lumps); $i++){
		$lumps[$i]["name"] = trim($lumps[$i]["name"]);
		switch($lumps[$i]["name"]){
			case "F_START":
			case "F1_START":
			case "F2_START":
			case "F3_START":
			case "FF_START":
				$cat = "Flat";
				break;
			case "P_START":
			case "P1_START":
			case "P2_START":
			case "P3_START":
			case "PP_START":
				$cat = "Patch";
				break;
			case "F_END":
			case "F1_END":
			case "F2_END":
			case "F3_END":
			case "FF_END":
				$cat = "Skip";
				break;
			case "P_END":
			case "P1_END":
			case "P2_END":
			case "P3_END":
			case "PP_END":
				$cat = "Skip";
				break;
		}
		if($lumps[$i]["size"] > 0) $lumps[$i]["category"] = $cat;
	}

	// WE'RE PRINTING IT FOR MERE DEBUG PURPOSES
	echo "<table>";
	echo "<tr><th>Pointer</th><th>Size</th><th>Lump Name</th><th>Lump Type</th></tr>";
	for($i = 0; $i < count($lumps); $i++){
		// EXTRACTS EVERY FLAT
		if($lumps[$i]["size"] > 0 && $lumps[$i]["category"] == "Flat"){
			$image = array();
			for($j = 0; $j < 64; $j++){
				$image[$j]["pixelInfo"] = getHexBE($lumps[$i]["pointer"] + ($j * 64), $lumps[$i]["pointer"] + (($j * 64) + 64), $final);
			}

			$gd = imagecreatetruecolor(64, 64);

			// WE'RE TURNING THE ARRAY FROM RGB VALUES TO ACTUAL COLOR OBJECTS TO BE USED BY PHP GD
			for($j = 0; $j < count($paletteArray); $j++){
				$colorArray[$j] = imagecolorallocate($gd, $paletteArray[$j]["r"], $paletteArray[$j]["g"], $paletteArray[$j]["b"]);
			}

			for($j = 0; $j < 64; $j++){
				for($k = 0; $k < strlen($image[$j]["pixelInfo"]); $k = $k + 2){
					$clr = hexdec($image[$j]["pixelInfo"][$k].$image[$j]["pixelInfo"][($k + 1)]);
					imagesetpixel($gd, ($k / 2), $j, $colorArray[$clr]);
				}
			}

			$wadName = substr($initWad, 0, strlen($initWad) - 4);
			if(!file_exists($wadName."/flats/")) mkdir($wadName."/flats/", 0777, true);

			$finalName = $wadName."/flats/".$lumps[$i]["name"].".png";
			imagepng($gd, $finalName);
			imagedestroy($gd);
		}

		// EXTRACTS EVERY PATCH
		if($lumps[$i]["size"] > 0 && $lumps[$i]["category"] == "Patch"){
			$image = array();
			// WIDTH (NUMBER OF COLUMNS), HEIGHT (NUMBER OF ROWS)
			$image[0]["w"] = getInt($lumps[$i]["pointer"], $lumps[$i]["pointer"] + 2, $final);
			$image[0]["h"] = getInt($lumps[$i]["pointer"] + 2, $lumps[$i]["pointer"] + 4, $final);
			// LEFT OFFSET, TOP OFFSET
			$image[0]["lo"] = getInt($lumps[$i]["pointer"] + 4, $lumps[$i]["pointer"] + 6, $final);
			$image[0]["to"] = getInt($lumps[$i]["pointer"] + 6, $lumps[$i]["pointer"] + 8, $final);
			$postInfo = $lumps[$i]["pointer"] + 8;
			for($j = 0; $j < $image[0]["w"]; $j++){
				$image[$j]["colPointer"] = $lumps[$i]["pointer"] + (getInt(($postInfo + ($j * 4)), (($postInfo + ($j * 4)) + 4), $final));
				$image[$j]["fromTop"] = getInt($image[$j]["colPointer"], $image[$j]["colPointer"] + 1, $final);
				$image[$j]["nPixels"] = getInt($image[$j]["colPointer"] + 1, $image[$j]["colPointer"] + 2, $final);
				for($k = 0; $k < $image[$j]["nPixels"]; $k++){
					// WE'RE IGNORING THE FIRST THREE BYTES BECAUSE THE FIRST ONE DETERMINES WHERE WE START DRAWING THE PIXELS FROM THE TOP, THE SECOND ONE DETERMINES THE NUMBER OF PIXELS WE'LL DRAW, AND THE THIRD ONE HAS NO DATA
					// WE'RE ALSO IGNORING THE LAST THREE BYTES BECAUSE THEY HOLD NO DATA AS WELL (THEY GIVE NO EXPLANATION ON THE DOOM BIBLE; THEY JUST SAY THE BYTES ARE TO BE IGNORED)
					// PIXELINFO CONTAINS THE PIXEL INFORMATION FOR EVERY PIXEL IN EACH COLUMN OF PIXELS
					$start = $image[$j]["colPointer"] + 3;
					$end = $image[$j]["colPointer"] + $image[$j]["nPixels"] + 3;
					$image[$j]["pixelInfo"] = getHexBE($start, $end, $final);
				}
			}

			$gd = imagecreatetruecolor($image[0]["w"], $image[0]["h"]);

			// WE'RE TURNING THE ARRAY FROM RGB VALUES TO ACTUAL COLOR OBJECTS TO BE USED BY PHP GD
			for($j = 0; $j < count($paletteArray); $j++){
				$colorArray[$j] = imagecolorallocate($gd, $paletteArray[$j]["r"], $paletteArray[$j]["g"], $paletteArray[$j]["b"]);
			}

			for($j = 0; $j < $image[0]["w"]; $j++){
				for($k = 0; $k < strlen($image[$j]["pixelInfo"]); $k = $k + 2){
					$clr = hexdec($image[$j]["pixelInfo"][$k].$image[$j]["pixelInfo"][($k + 1)]);
					imagesetpixel($gd, $j, ($k / 2), $colorArray[$clr]);
				}
			}

			$wadName = substr($initWad, 0, strlen($initWad) - 4);
			if(!file_exists($wadName."/patches/")) mkdir($wadName."/patches/", 0777, true);

			$finalName = $wadName."/patches/".$lumps[$i]["name"].".png";
			imagepng($gd, $finalName);
			imagedestroy($gd);
		}
		if($lumps[$i]["size"] > 0 && $lumps[$i]["category"] != "" && $lumps[$i]["category"] != "Skip")
			echo "<tr><td>".$lumps[$i]["pointer"]."</td><td>".$lumps[$i]["size"]."</td><td>".$lumps[$i]["name"]."</td><td>".$lumps[$i]["category"]."</td></tr>";
	}
	echo "</table>";

	// OLD, IMPRACTICAL STUFF, BUT GOOD FOR DEBUGGING

	// DISPLAY A PATCH MADE OF DIVS
	/*for($i = 0; $i < $image[0]["w"]; $i++){
		for($j = 0; $j < strlen($image[$i]["pixelInfo"]); $j = $j + 2){
			$clr = hexdec($image[$i]["pixelInfo"][$j].$image[$i]["pixelInfo"][($j + 1)]);
			// top: ($j / 2)px; left: ($i)px;
			echo "<div style='width: 1px; height: 1px; display: inline-block; background-color: rgb(".$paletteArray[$clr]["r"].", ".$paletteArray[$clr]["g"].", ".$paletteArray[$clr]["b"].")'></div>";
		}
		echo "<br>";
	}*/

	// DISPLAY A FLAT MADE OF DIVS
	/*for($i = 0; $i < 64; $i++){
		for($j = 0; $j < strlen($image[$i]["pixelInfo"]); $j = $j + 2){
			$clr = hexdec($image[$i]["pixelInfo"][$j].$image[$i]["pixelInfo"][($j + 1)]);
			echo "<div style='width: 1px; height: 1px; display: inline-block; background-color: rgb(".$paletteArray[$clr]["r"].", ".$paletteArray[$clr]["g"].", ".$paletteArray[$clr]["b"].")'></div>";
		}
		echo "<br>";
	}*/
?>