<?php
	function pr($var){
		echo "<pre>"; print_r($var); echo "</pre>";
	}

	function getScheduleFromTime($origin, $dep_time){
		$url = "https://maps.googleapis.com/maps/api/directions/json";
		$fields = array(
			"origin" => $origin,
			"destination" => "41.188205,-8.685123",
			"mode" => "transit",
			"departure_time" => $dep_time,
			"key" => "AIzaSyDAegSuFzIhGUcbQ158hG99rE-61LoqYQo"
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url."?".http_build_query($fields));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		$res = curl_exec($ch);
		$dInfo = json_decode($res);

		//close connection
		curl_close($ch);

		//FORMATO: text = HH:nn, time_zone = Europe/Lisbon, value = text em UNIX time
		return $dInfo->routes[0]->legs[0]->departure_time;
	}

	function getNextStops($origin, $n){
		//Cria a data actual e a data do próximo metro
		$a = new DateTime();
		$p = new DateTime();
		$info = getScheduleFromTime($origin, $p);
		//Atribui ao valor da data do próximo metro o seu valor em UNIX timestamp
		$p->setTimestamp($info->value);
		//Calcula a diferença entre as duas datas
		$d = date_diff($a, $p)->format("%i");
		echo $d." minutos<br>";
		for($i = 1; $i < $n; $i++){
			$info = getScheduleFromTime($origin, $info->value + 1);
			
			$p->setTimestamp($info->value);
			$d = date_diff($a, $p)->format("%i");
			echo $d." minutos<br>";
		}
	}

	//pr(getScheduleFromTime("41.183452,-8.553955", "now"));
	getNextStops("41.183452,-8.553955", 5);
?>