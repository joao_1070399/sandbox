
var itinerarium = new function () {

	var sig = {
		storage: {
			call: $
		},
		properties: {
			call: {
				url: "/pt/itinerarium/callservice.php",
				options: {
					type: "get",
					dataType: "text"
				},
				callbackFail: $.noop
			},
			services: {
				walk: 		{ id: 0, mode: 0, is: true, name: "A pé", 		color: "#666666", mark: "iconParagemW.png", icon: "" },
				stcp: 		{ id: 1, mode: 1, is: true, name: "Autocarro", 	color: "#074D81", mark: "iconParagem.png", 	icon: "Pictograma_Autocarro.png" },
				eletrico: 	{ id: 0, mode: 2, is: true, name: "Elétrico", 	color: "#074D81", mark: "iconParagem.png", 	icon: "Pictograma_Electrico.png" },
				metro:	 	{ id: 2, mode: 3, is: true, name: "Metro", 		color: "#00225C", mark: "iconParagemM.png", icon: "Pictograma_Metro.png" },
				cp: 		{ id: 3, mode: 4, is: true, name: "Comboio", 	color: "#05902B", mark: "iconParagemC.png", icon: "Pictograma_Comboio.png" },
				funicular: 	{ id: 0, mode: 5, is: true, name: "Funicular", 	color: "#074D81", mark: "iconParagemM.png", icon: "Pictograma_Funicular.png" }
			},
			schedule: {
				date: 		"NODATE",
				departure: 	"NOTIME",
				arrival: 	"NOTIME"
			},
			radius: 500,
			walk: 600,
			route: -1
		},
		methods: {
			warn: function (message) {
				
				return console.warn("[Itinerarium]", message);
				
			},
			error: function (message) {
				
				return console.error("[Itinerarium]", message);
				
			},
			init: function () {

				
				
			},
			call: function (parameters, callback) {
			
				if (typeof sig.storage.call == "object") {
					if ("abort" in sig.storage.call) {
						sig.storage.call.abort();
					}
				}
				
				var complete = function (data) {
					
					try  {
						data = $.parseJSON(data);
					} catch (e) {
						
					}
					
					callback(data);
					
				}
				
				var options = $.extend(
					sig.properties.call.options,
					{
						url: sig.properties.call.url,
						data: parameters
					}
				);
				
				sig.storage.call = $.ajax(options);
				
				sig.storage.call.done(function (data) {
					complete(data);
				});
				
				sig.storage.call.fail(sig.properties.call.callbackFail);
			
			},
			parse: {
				multipoint: function (data) {
					
					data = $.trim(data).split(",")[0].replace(/\s/g, ',').replace(/[^0-9\.\-\,]/g, '');
				
					return data;
				
				},
				placesByName: function (data) {
					
					if (!data) {
						return [];
					}
					
					return $.map(data.records, function (item) {
						return $.extend(item, {
									label: item.nome,
									value: sig.methods.parse.multipoint(item.geom)
								});
					})
				
				},
				lines: function (data) {
					
					if (!data["records"]) {
						return [];
					}
					
					return $.map(data.records, function (item) {
						return $.extend(item, {
										label: "$0 [$1]".format(item.description, item.code),
										value: item.code
									});
					});
				},
				linesDir: function (data) {
					
					if (!data["records"]) {
						return [];
					}
					
					return $.map(data.records, function (item) {
						return $.extend(item, {
										label: item.descr_dir,
										value: item.dir
									});
					});
				},
				linesByName: function (data) {
					
					if (!data) {
						return [];
					}
					
					return $.map(data, function (item) {
						return $.extend(item, {
										label: "$0 [$1]".format(item.name, item.code),
										value: item.code,
										geomdesc: eval("($0)".format(item.geomdesc))
									});
					});
				},
				busStopByCode: function (data) {
					
					if (!data) {
						return [];
					}
					
					return $.map(data, function (item) {
						return $.extend(item, {
										label: "$0 [$1]".format(item.name, item.code),
										value: item.code,
										geomdesc: eval("($0)".format(item.geomdesc))
									});
					});
				},
				routeAdd: function (data) {
					
					data = $.trim(data.replace(/next\s([0-9]+)(.*)/g, "$1"));
				
					return data;
					
				}
			},
			radius: {
				get: function () {
				
					return sig.properties.radius
				
				},
				set: function (value) {
					
					sig.properties.radius = Math.abs(parseInt(value));
				
				}
			},
			schedule: {
				get: {
					date: function () {
						return sig.properties.schedule.date;
					},
					departure: function () {
						return sig.properties.schedule.departure;
					},
					arrival: function () {
						return sig.properties.schedule.arrival;
					}
				},
				set: {
					date: function (value) {
						if (!value) {
							value = "NODATE";
						}
						sig.properties.schedule.date = value;
					},
					departure: function (value) {
						if (!value) {
							value = "NOTIME";
						}
						sig.properties.schedule.departure = value;
					},
					arrival: function (value) {
					if (!value) {
							value = "NOTIME";
						}
						sig.properties.schedule.arrival = value;
					}
				}
			},
			services: {
				verify: function (service) {
				
					if (!(service in sig.properties.services)) {
						sig.methods.warn("Service '" + service + "' doesn't exist.");
						
						return false;
					}
					
					return true;
				
				},
				get: function (service) {
				
					if (!sig.methods.services.verify(service)) {
						return false;
					}
					
					return sig.properties.services[service].is;
				
				},
				getById: function (id) {
					
					for(var i in sig.properties.services) {
						if (sig.properties.services[i].id.toString() == id.toString()) {
							return sig.properties.services[i];
						}
					}
					
					return sig.properties.services.stcp;
				
				},
				getByMode: function (mode) {
					
					for(var i in sig.properties.services) {
						if (sig.properties.services[i].mode.toString() == mode.toString()) {
							return sig.properties.services[i];
						}
					}
					
					return sig.properties.services.stcp;
				
				},
				set: function (service, value) {
					
					if (!sig.methods.services.verify(service)) {
						return false;
					}
					
					sig.properties.services[service].is = value ? true : false;
					
					return true;
					
				},
				one: function () {
					
					for (i in sig.properties.services) {
						if (sig.properties.services[i].is == true && sig.properties.services[i].id > 0) {
							return sig.properties.services[i].id;
						}
					}
					
					return 0;
				
				},
				only: function (service) {
					
					if (!sig.methods.services.verify(service)) {
						return false;
					}
					
					for (i in sig.properties.services) {
						sig.properties.services[i].is = false;
					}
					
					sig.properties.services[service].is = true;
					
					return true;
					
				}
			},
			list: {
				sellers: function (parameter, callback) {
				
					var position = parameter.split(",");
				
					var parameters = {
						"action":	"pontosvenda",
						"posx": 	position[0],
						"posy": 	position[1], 
						"radius":	sig.properties.radius
					}
					
					sig.methods.call(parameters, callback);
				
				},
				placesByName: function (parameter, source) {
				
					var parameters = {
						"action":	"srchlocbname",
						"qrystr": 	parameter,
						"fltbus": 	sig.methods.services.get("stcp"),
						"fltmetro": sig.methods.services.get("metro"), 
						"fltcp": 	sig.methods.services.get("cp"),
						"flttop": 	true,
						"fltpoi": 	true
					}
					
					var callback = function (data) { 
						return source.feeder(sig.methods.parse.placesByName(data)); 
					}
					
					sig.methods.call(parameters, callback);
				
				},
				lines: function (parameter, source) {
				
					var parameters = {
						"action":	"lineslist",
						"name":		parameter,
						"service": 	sig.methods.services.one()
					}
				
					var callback = function (data) { 
						return source.feeder(sig.methods.parse.lines(data)); 
					}
				
					sig.methods.call(parameters, callback);
				
				},
				linesDir: function (parameter, sourec) {
				
					var parameters = {
						"action":	"srchstoplines",
						"stopname": parameter
					}
					
					var callback = function (data) { 
						return source.feeder(sig.methods.parse.linesDir(data)); 
					}
					
					sig.methods.call(parameters, callback);
				
				},
				linesByName: function (parameter, source) {
				
					var parameters = {
						"action":	"srchstoplines",
						"stopname": parameter
					}
					
					var callback = function (data) { 
						return source.feeder(sig.methods.parse.linesByName(data)); 
					}
					
					sig.methods.call(parameters, callback);
				
				},
				busStopByCode: function (parameter, source) {
				
					var parameters = {
						"action":	"srchstoplines",
						"stopcode": parameter
					}
					
					var callback = function (data) {
						return source.feeder(sig.methods.parse.busStopByCode(data)); 
					}
					
					sig.methods.call(parameters, callback);
				
				},
				linesByCoordinates: function (parameter, callback) {
					
					var position = parameter.split(",");
					
					var parameters = {
						"action":	"routesnearpoint",
						"posx": 	position[0],
						"posy": 	position[1],
						"radius":	sig.properties.radius
					}
					
					sig.methods.call(parameters, callback);
					
				},
				stopsByCoordinates: function (parameter, callback) {
					
					var position = parameter.split(",");
					
					var parameters = {
						"action":	"stopsnearpoint",
						"x": 	position[0],
						"y": 	position[1],
						"maxcost ":	sig.properties.walk
					}
					
					sig.methods.call(parameters, callback);
					
				},
				typesOfSchedule: function (source) {

					var parameters = {
						"action":	"timetableepochs",
						"service": 	sig.properties.services.stcp.id
					}

					var callback = function (data) { 
						return source.feeder(data, source.callback); 
					}

					sig.methods.call(parameters, callback);
				}
			},
			spots: {
				tooltip: function(title, code) {
				
					var tooltip = $("<div></div>");
					var ttitle 	= $("<div></div>").html(title);
					var tocde 	= $("<div></div>").html(code);
					
					
					return tooltip;
				
				}
			},
			routes: {
				submitAll: function (parameter, callback) {
					
					var parameters = {
						"action":	"single-odresult",
						"args":		parameter,
						"date":		sig.methods.schedule.get.date(),
						"deptime":	sig.methods.schedule.get.departure(),
						"arrivtime":sig.methods.schedule.get.arrival(),
						"ma":		sig.methods.services.get("stcp"),
						"mb":		sig.methods.services.get("metro"),
						"mc":		sig.methods.services.get("cp")
					}
					
					sig.methods.call(parameters, callback);
				
				},
				traceAll: function (parameter, callback) {
					
					var parameters = {
						"action":	"single-odroadbook",
						"reqid":	parameter.reqid,
						"orderreq":	parameter.orderreq,
						"ordersol":	parameter.order
					}
					
					sig.methods.call(parameters, callback);
				
				},
				lineDraw: function (parameter, callback) {
					
					var parameters = {
						"action":	"linedraw",
						"lcode": 	parameter.lcode,
						"ldir": 	parameter.ldir
					}
					
					sig.methods.call(parameters, callback);
					
				},
				infowindow: function (parameter, callback) {
				
					var parameters = {
						"action":	"single-infowindow",
						"code": 	parameter
					}
					
					sig.methods.call(parameters, callback);
					
				},
				draw: function (data, line, optionsLine, optionsMark) {
				
					if (data) {
					
						if (data.route) {
						
							for (var i in data.route) {
								
								var points = new Array();
								
								try {
								
									points = $.parseJSON(data.route[i].geomdesc);
								
								} catch (e) { 
									sig.methods.error(e);
								}
								
								var color = sig.methods.services.getByMode(data.route[i].mode).color;
								
								if (points.type.contains("MultiLineString")) {
								
									for(j in points.coordinates) {
									
										var npoints = new Array();
											
										for(k in points.coordinates[j]) {
											npoints.splice(npoints.length, 0, new google.maps.LatLng(points.coordinates[j][k][1], points.coordinates[j][k][0]));
										}
										
										framework.components.maps.methods.polys.add(
											$.extend({
												"path": 		npoints,
												"tag":			"markers,lines,line" + line + ",api-" + i + ",api2-" + j,
												"strokeColor":  color,
												"strokeOpacity": 0.85,
												"strokeWeight": 4
											}, optionsLine)
										);
									
									}
								
								} else {
								
									var npoints = new Array();
									
									for(var j in points.coordinates) {
										npoints.splice(npoints.length, 0, new google.maps.LatLng(points.coordinates[j][1], points.coordinates[j][0]));
									}
									
									framework.components.maps.methods.polys.add(
										$.extend({
											"path": 		npoints,
											"tag":			"markers,lines,line" + line + ",api-" + i,
											"strokeColor": 	color,
											"strokeOpacity": 0.85,
											"strokeWeight": 4
										}, optionsLine)
									);
								}
							}
						
						}
						
						if (data.locations) {
							
							for(var i in data.locations) {
								
								var points = new Array();
								var service = sig.methods.services.getById(data.locations[i].service);
								
								try {
								
									points = $.parseJSON(data.locations[i].geomdesc);
								
								} catch (e) {
									//sig.methods.error(e);
								}
								
								if (points["coordinates"]) {
									framework.components.maps.methods.markers.add(
										$.extend({
											"title": 	data.locations[i].label,
											"tag":		"markers,lines,line" + line + ",api-" + i,
											"bounds": 	true,
											"position":	new google.maps.LatLng(points.coordinates[1], points.coordinates[0]),
											"icon":		"/temas/stcp/imgs/$0".format(service.mark),
											"zIndex": 	3,
											"ttCode":   data.locations[i].code,
											"ttHtml":   data.locations[i]["ttHtml"]
										}, optionsMark)
									);
								}
								
							}
							
						}
						
					}
				
				},
				// ALL THE BELLOW FUNCTIONS ARE DEPRECATED
				roadbook: function (parameter, callback) {
					
					var parameters = {
						"action":	"odroadbook",
						"reqid":	parameter.reqid,
						"orderreq":	parameter.orderreq,
						"ordersol":	parameter.order
					}
					
					sig.methods.call(parameters, callback);
				
				},
				geometry: function (parameter, callback) {
				
					var parameters = {
						"action":	"odgeometry",
						"reqid":	parameter.reqid,
						"orderreq":	parameter.orderreq,
						"ordersol":	parameter.order
					}
					
					sig.methods.call(parameters, callback);
				
				},
				add: function (parameter, source) {
				
					if (!parameter["reqid"]) {
						parameter["reqid"] = -1;
					}
					
					var parameters = {
						"action":	"odpoint",
						"reqid": 	parameter.reqid,
						"x": 		parameter.x,
						"y": 		parameter.y
					}
					
					var callback = function (data) {
						return source.callback(sig.methods.parse.routeAdd(data)); 
					}
					
					sig.methods.call(parameters, callback);
					
				},
				calculate: function (callback) {
				
					var parameters = {
						"action":	"odreqcalc",
						"reqid": 	sig.properties.route
					}
					
					sig.methods.call(parameters, callback);
				
				},
				list: function (callback) {
				
					var parameters = {
						"action":	"odresult",
						"reqid": 	sig.properties.route
					}
					
					sig.methods.call(parameters, callback);
				
				}
			}
		},
		events: {
		
		
		}	
	}

	sig.methods.init();
	
	var framework = {
		storage: { },
		properties: { },
		methods: {
			warn: function (message) {
				
				sig.methods.warn(message);
				
			},
			error: function (message) {
				
				sig.methods.error(message);
				
			},
			init: function () {
			
				framework.components.maps.methods.init();
				framework.components.autocomplete.methods.init();
			
			}
		},
		events: { },
		components: {
			api: sig,
			maps: {
				storage: {
					core: 		$,
					geo: 		new google.maps.Geocoder(),
					polys: 		new Array(),
					markers: 	new Array(),
					bounds: 	new google.maps.LatLngBounds(),
					tooltip:	new google.maps.InfoWindow()
				},
				properties: {
					options: {
						mapTypeId: 	google.maps.MapTypeId.ROADMAP,
						center: 	new google.maps.LatLng(41.149932, -8.610713),
						zoom: 		13,
						streetViewControl: true
					}
				},
				methods: {
					init: function () {
						
						
						
					},
					parse: {
						spotName: function (name) {
							return name.replace(/[^a-zA-Z0-9\_]/g, ""); 
						}
					},
					set: function (element, options) {
						
						if (options) {
							framework.components.maps.properties.options = $.extend(framework.components.maps.properties.options, options);
						}
						
						framework.components.maps.storage.core = new google.maps.Map(
							$(element)[0],
							framework.components.maps.properties.options
						);
						
						if (options) {
							if (typeof options["dblclick"] == "function") {
								google.maps.event.addListener(
									framework.components.maps.storage.core, 
									"dblclick", 
									options.dblclick 
								);
							}
						}
						
					},
					center: function () {
						
						google.maps.event.trigger(
							framework.components.maps.storage.core, 
							"resize"
						);
						
						var range = new Array();
						var markers = framework.components.maps.storage.markers;
						var polys = framework.components.maps.storage.polys;
						
						framework.components.maps.storage.bounds = new google.maps.LatLngBounds();
						
						if (markers.length > 0) {
							for (i in markers) {
								if (markers[i].visible == true) {
									range.splice(-1, 0, markers[i].position);
								}
							}
						}
						
						if (range.length > 1) {
						
							for (i in range) {
								framework.components.maps.storage.bounds.extend(range[i]);
							}
							
							framework.components.maps.storage.core.fitBounds(
								framework.components.maps.storage.bounds
							);
							
						} else {
							
							framework.components.maps.storage.core.setOptions({
								zoom: framework.components.maps.properties.options.zoom
							});
						
							framework.components.maps.storage.core.panTo(
								range.length == 1 ? 
								range[0] :
								framework.components.maps.properties.options.center
							);
						
						}
						
					},
					coordinates: {
						getName: function(x, y, callback) {
							
							framework.components.maps.storage.geo.geocode({ 
								
								"latLng": new google.maps.LatLng(x, y) 
							
							}, function(results, status) {
								
								if (!results.length) return false;
								
								callback({ 
									"x": x, 
									"y": y, 
									"address": results[0].formatted_address 
								});
								
							});
						
						}
					},
					polys: {
						add: function (options) {
							
							var poly = new google.maps.Polyline(options);
							
							framework.components.maps.storage.polys.splice(-1, 0, poly);
							
							poly.setMap(framework.components.maps.storage.core);
							
						},
						remove: function (tag) {
						
							if (framework.components.maps.storage.polys.length > 0) {
								for (i = 0; i < framework.components.maps.storage.polys.length; i++) {
									var tags = framework.components.maps.storage.polys[i].tag.split(",");
									if ($.inArray(tag, tags) > -1) {
										framework.components.maps.storage.polys[i].setMap(null);
										framework.components.maps.storage.polys.splice(i, 1);
										i--;
									}
								}
							}
							
						},
						visible: function (tag, value) {
							
							if (framework.components.maps.storage.polys.length > 0) {
								for (i = 0; i < framework.components.maps.storage.polys.length; i++) {
									var tags = framework.components.maps.storage.polys[i].tag.split(",");
									if ($.inArray(tag, tags) > -1) {
										framework.components.maps.storage.polys[i].setVisible(value);
									}
								}
								
								framework.components.maps.methods.center();
								
							}
						},
						exist: function (tag) {
							
							if (framework.components.maps.storage.polys.length > 0) {
								for (i = 0; i < framework.components.maps.storage.polys.length; i++) {
									var tags = framework.components.maps.storage.polys[i].tag.split(",");
									
									if ($.inArray(tag, tags) > -1) {
										return true;
									}
								}
							}
							
							return false;
							
						},
						clear: function () {
						
							if (framework.components.maps.storage.polys.length > 0) {
								for (i in framework.components.maps.storage.polys) {
									framework.components.maps.storage.polys[i].setMap(null);
								}
								framework.components.maps.storage.polys = new Array();
							}
							
						}
					},
					tooltip: function (html, position, callback) {
					
						framework.components.maps.storage.tooltip.setContent(html);
						framework.components.maps.storage.tooltip.setPosition(position);
						
						framework.components.maps.storage.tooltip.open(framework.components.maps.storage.core);

						if (typeof callback == "function") {
							callback();
						}
						
					},
					markers: {
						add: function (options, callback) {
							
							options = 	$.extend({ 
											"visible": true,
											"bounds": true
										}, options); 
							
							var marker = new google.maps.Marker(options);
							
							framework.components.maps.storage.markers.splice(-1, 0, marker);
							
							if (options["draggable"]) {
								google.maps.event.addListener(marker, "dragend", function (e) {

									if (typeof callback == "function") {
										framework.components.maps.methods.coordinates.getName(e.latLng.lat(), e.latLng.lng(), callback);
									}
								
								});
							}
							if (options["tooltip"]) {
								google.maps.event.addListener(marker, "click", function(e) {
									options.tooltip(marker);
									return true;
								});
							}
							if (typeof options["click"] === "function") {
								google.maps.event.addListener(marker, "click", options.click);
							}
							if (typeof options["mouseover"] === "function") {
								google.maps.event.addListener(marker, "mouseover", options.mouseover);
							}
							if (typeof options["mouseout"] === "function") {
								google.maps.event.addListener(marker, "mouseout", options.mouseout);
							}
							
							marker.setMap(framework.components.maps.storage.core);
							
							framework.components.maps.methods.center();
							
						},
						remove: function (tag) {
						
							framework.components.maps.storage.tooltip.close();
							
							if (framework.components.maps.storage.markers.length > 0) {
								for (i = 0; i < framework.components.maps.storage.markers.length; i++) {
									var tags = framework.components.maps.storage.markers[i].tag.split(",");
									if ($.inArray(tag, tags) > -1) {
										framework.components.maps.storage.markers[i].setMap(null);
										framework.components.maps.storage.markers.splice(i, 1);
										i--;
									}
								}
								
								framework.components.maps.methods.center();
							}
						},
						visible: function (tag, value) {
							
							framework.components.maps.storage.tooltip.close();
							
							if (framework.components.maps.storage.markers.length > 0) {
								for (i = 0; i < framework.components.maps.storage.markers.length; i++) {
									var tags = framework.components.maps.storage.markers[i].tag.split(",");
									if ($.inArray(tag, tags) > -1) {
										framework.components.maps.storage.markers[i].setVisible(value);
									}
								}
								
								framework.components.maps.methods.center();
								
							}
						},
						exist: function (tag) {
							
							if (framework.components.maps.storage.markers.length > 0) {
								for (i = 0; i < framework.components.maps.storage.markers.length; i++) {
									var tags = framework.components.maps.storage.markers[i].tag.split(",");
									if ($.inArray(tag, tags) > -1) {
										return true;
									}
								}
							}
							
							return false;
							
						},
						clear: function () {
						
							framework.components.maps.storage.tooltip.close();
						
							if (framework.components.maps.storage.markers.length > 0) {
								for (i in framework.components.maps.storage.markers) {
									framework.components.maps.storage.markers[i].setMap(null);
								}
								framework.components.maps.storage.markers = new Array();
								framework.components.maps.methods.center();
							}

						}
					}
				}
			},
			autocomplete: {
				storage: {
					
				},
				properties: {
					classNames: {
						uiAutoCompleteEmtpy: "ui-autocomplete-input-empty",
						uiAutoCompleteLoading: "ui-autocomplete-loading"
					}
				},
				methods: {
					init: function () {
					
					},
					add: function (element, method, options) {
					
						var elements = new Array();
					
						$(element).each(function (i) {
							
							var main = $(this);
							
							if (main.data("ac-rel")) {
							
								if (!main.data("autocomplete")) {
									main = $(main.data("ac-rel"));
								}
								
								if (main.data("autocomplete")) {
									
									var options2 = $.extend(options, {
									
										"source": function (search, callback) {
										
											var source = {
												"element": 	$(this.element),
												"method":	method,
												"search": 	search.term,
												"feeder": 	framework.components.autocomplete.methods.feeder,
												"callback": callback
											}
										
											framework.components.autocomplete.methods.call(source);
										
										}
										
									});
								
									$(this).autocomplete("option", options2);
									
									return;
									
								}
							}
							
							var list = $("<input type='text' />").attr({
											"class": main.attr("class"),
											"id": main.attr("id")
										});
							
							main.removeAttr("id")
							list.insertBefore(main);
							
							if (main.attr("type") != "hidden") {
								main.remove();
								main = 	$("<input type='hidden' />").attr({
											"name": 	main.attr("name"),
											"value": 	main.attr("value")
										});
								main.insertBefore(list);
							}
							
							main.data("ac-rel", list);
							list.data("ac-rel", main);
							
							list.autocomplete({
								minLength: 2,
								delay: 750,
								source: function (search, callback) {
									
									var source = {
										"element": 	$(this.element),
										"method":	method,
										"search": 	search.term,
										"feeder": 	framework.components.autocomplete.methods.feeder,
										"callback": callback
									}
								
									framework.components.autocomplete.methods.call(source);
									
								},
								select: function(event, ui) {
									
									list.val(ui.item.label);
									main.val(ui.item.value);
									
									if (options) {
										if (options["select"]) {
											if (typeof options.select == "function") {
												options.select(event, ui, list);
											}
										}
									}

									return false;
									
								},
								change: function (event, ui, o) {
									
									if (list.data("autocomplete")["cancelChange"] == true) {
										list.data("autocomplete")["cancelChange"] = false;
										return false;
									}
									
									
									if (!ui.item) {
										
										main.val("");
										
										if (list.val()) {
											$(this).addClass(framework.components.autocomplete.properties.classNames.uiAutoCompleteEmtpy);
										}
										
									}
									
									if (options) {
										if (options["change"]) {
											if (typeof options.change == "function") {
												options.change(event, ui, list);
											}
										}
									}
									
									return false;
									
								},
								close: function (event, ui) {
									
									if (!ui["item"] && !main.val()) {
										
										$(this).data("autocomplete")["selectedItem"] = $(this).data("autocomplete")["sourceData"][0];
										
										ui["item"] = $(this).data("autocomplete")["selectedItem"];
										
										$(this).data("autocomplete").options.select(event, ui);
									}
								
								},
								focus: function (event, ui) {
									
									if (options) {
										if (options["focus"]) {
											if (typeof options.focus == "function") {
												options.focus(event, ui, list);
											}
										}
									}
									
									return false;
								
								}
							}).bind("keydown", framework.components.autocomplete.events.keydown)
							
							elements[i] = list[0];
						
						});
						
						return $(elements);
						
					},
					remove: function (element) {
					
						$(element).data("ac-rel").remove();
						$(element).remove();
					
					},
					call: function (source) {
					
						if (!(source.method in sig.methods.list)) {
							framework.methods.error("Method '" + source.method + "' doesn't exist.");
							return false;
						}
						
						sig.methods.list[source.method](source.search, source);
					
					},
					feeder: function(data) {
						
						$(this.element).data("autocomplete")["sourceData"] = data;
						
						$($(this.element).data("ac-rel")).val("");
						$(this.element).removeClass(framework.components.autocomplete.properties.classNames.uiAutoCompleteEmtpy);
						
						this.callback(data);
						
						if (data.length == 0) {
							
							$(this.element).addClass(framework.components.autocomplete.properties.classNames.uiAutoCompleteEmtpy);
							
						}
					
					}
				},
				events: {
					keydown: function (e) {
						
						$(this).removeClass(framework.components.autocomplete.properties.classNames.uiAutoCompleteEmtpy);
					
					}
				}
			}
		}
	}
	
	framework.methods.init();
	
	return framework.components;

}
