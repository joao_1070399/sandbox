
var sig = new function()
{
	this.URL = '/itinerarium/callservice.php?action='; 
    
    this.getAllLinhas = function(el)     {
        return makeCall(this.URL + "lineslist", {service : 1}, "Não foi possível obter as linhas.", "buildLinhas", "json", el);
    }
    
    this.getSentidoByLinha = function(codigo, el) {
        return makeCall(this.URL + "linedirslist", {lcode:codigo}, "Não foi possível obter os sentidos.", "buildSentidos", "json", el);
    }

    this.getParagensByLinhaAndSentido = function(codigo, sentido, el, callback_fn) {
		callback_fn = (callback_fn) ? callback_fn : 'buildParagens';
        return makeCall(this.URL + "linestops", {lcode:codigo, ldir:sentido}, "Não foi possível obter as paragens.", callback_fn, "json", el);
    }

    this.getParagemInfo = function(codigo_paragem, linha) {
        return makeCall("/itinerarium/soapclient.php", {codigo: codigo_paragem, linha: linha}, 'Não foi possível obter a informação da paragem', "outputparagem", 'html');
    }

	this.getBusStopList = function(nome_paragem, el) {
		return makeCall(this.URL + "srchstoplines", {stopname: nome_paragem, service: 1}, "Não foi possível obter as paragens.", "buildBusStopList", "json", el);
	}
    
    function makeCall(uri, params, msg, callback_fn, data_type, el)
    {
        data_type = (data_type == undefined || data_type == null) ? 'json' : data_type;
        
        $.blockUI({message: '<div class="blockUILoading"><img src="/temas/stcp/imgs/loading-big-color.gif" /></div>',
			css: { 
                border: 'none', 
                padding: '10px', 
                backgroundColor: 'none',
                color: '#FFF' 
            },
			overlayCSS:  { 
				backgroundColor: '#FFF', 
				opacity: 0.8 
			}
        });
        $.ajax({
            url: uri,
            dataType: data_type,
            data: params
        }).done(function( list ) {
            var fn = window[callback_fn];
            fn(list, el);
        }).fail(function(){
            alert(msg);
        });
    }
}

function buildLinhas(linhas, el) {
    if(linhas.records.length > 0) {
        for(var i=0; i<linhas.records.length; i++) {
            var option = $("<option></option>");
            option.text(linhas.records[i].description);
            option.attr("value", linhas.records[i].code);
            $(el).append(option);
        }
    }
}

function buildSentidos(sentidos, el) {
    if(sentidos.records.length > 0)
    {
        for(var i=0; i<sentidos.records.length; i++)
        {
            var option = $("<option></option>");
			option.text(sentidos.records[i].descr);
            option.attr("value", sentidos.records[i].dir);
            $(el).append(option);
        }
    }
}

function buildParagens(paragens, el) {
    if(paragens.records.length > 0) {
        for(var i=0; i<paragens.records.length; i++) {
            var option = $("<option></option>");
            option.text(paragens.records[i].name + " (" + paragens.records[i].code + ")");
            option.attr("value", paragens.records[i].code);

            $(el).append(option);
        }
    }
}

function buildParagensByLinhaAndSentido(paragens, el) {
	
	$.ajax({
		type: 'post',
		url: '/xmlhttprequests/linhas/buildParagensByLinhaAndSentido.php',
		data: {paragens: paragens}
	}).done(function(msg) {
		$('#bus-stop-results').html(msg);
		$('#bus-stop-results').trigger('focus');
	}).fail(function(msg){
		alert(msg)
	});
}

function outputparagem(info) {
    $("#paragem_info_result").html(info);
}

function buildBusStopList(paragens, el) {
	var optgroup;
	if (paragens.length) {
		$.each(paragens, function(key, paragem) {
			optgroup = $("<option value='"+ paragem.code +"'>"+    paragem.name + " ("+ paragem.code +")"    +"</option>");			
			$(el).append(optgroup);
		});
	}
}