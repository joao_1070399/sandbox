<?php
	function pr($var){
		echo "<pre>"; print_r($var); echo "</pre>";
	}

	function getParagemInfo($paragem){
		//==================================COPIA DO POST PARA STCP
		//$url = 'http://www.stcp.pt/pt/itinerarium/callservice.php?action=lineslist';
		$url = 'http://www.stcp.pt/pt/itinerarium/soapclient.php';
		$fields = array(
			'codigo' => $paragem,
			'linha' => '0'
		);

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url."?".http_build_query($fields));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_POST, true);
		//curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);


		//execute post
		$res = curl_exec($ch);
		include_once("phpQuery-onefile.php");

		$arrInfo = Array();
		$info = phpQuery::newDocumentHTML($res)->find("#smsBusResults tr.even");

		foreach(pq($info) as $i => $linha){
			foreach(pq($linha)->find("td") as $j => $cel){
				switch($j){
					case 0:				
						$arrInfo[$i]["sentido"] = preg_replace("/\s/", "", trim(pq($cel)->text()));
						break;
					case 1:
						$arrInfo[$i]["hora"] = trim(pq($cel)->text());
						break;
					case 2:
						$arrInfo[$i]["falta"] = trim(pq($cel)->text());
						break;
				}
			}
		}

		//close connection
		curl_close($ch);

		echo str_replace("\u00a0", " ", json_encode($arrInfo));
	}

	getParagemInfo("RTE1");
?>