<?php
	function pr($v){
		echo "<pre>"; print_r($v); echo "</pre>";
	}

	function ripPage($url){
		//The information that wil be sent to the front-end
			$finalInfo = Array();

		//Gets the HTML code from the page and parses it as text
			$rawInfo = file_get_contents($url);
			$html = html_entity_decode($rawInfo);

		//Gets the artist name
			$firstTrim = explode('artist: "', $html)[1];
			$secondTrim = explode('"', $firstTrim)[0];

			$albumArtist = utf8_encode($secondTrim);
			$finalInfo["albumInfo"]["albumArtist"] = $albumArtist;

		//Gets the album and its information
			$firstTrim = explode("current: ", $html)[1];
			$secondTrim = explode("},", $firstTrim)[0]."}";

			$secondTrim = utf8_encode($secondTrim);

			$albumInfo = json_decode($secondTrim, true);
			$finalInfo["albumInfo"] = $albumInfo;

			// Gets its cover art
				$firstTrim = explode('artFullsizeUrl: "', $html)[1];
				$secondTrim = explode('"', $firstTrim)[0];

				$albumCover = utf8_encode($secondTrim);
				$finalInfo["albumInfo"]["albumCover"] = $albumCover;

		//Gets every track with its information and (most importantly) download links
			$firstTrim = explode("trackinfo : ", $html)[1];
			$secondTrim = explode("],", $firstTrim)[0]."]";

			$secondTrim = utf8_encode($secondTrim);

			$albumTracks = json_decode($secondTrim, true);
		
		foreach ($albumTracks as $track) {
			$finalInfo["tracks"][] = $track;
		}

		echo json_encode($finalInfo);
	}

	
	if(isset($_GET["url"])) ripPage($_GET["url"]);
?>