var pool = Array();
var deck = Array();
var lastSortP = "default";
var lastSortD = "color";

function generateSealed(){
	$.get("engine.php", function(){ console.log("success"); })
	.done(function(data){
		data = JSON.parse(data);
		// CREATES A PERSISTENT POOL, TO SORT AND ALTER LATER
		pool = data;
		printCards(pool, "pool");
	});
}

function printCards(cards, w){
	var slotId = 0;
	$("#"+w+" .slot").remove();
	if($("#"+w).children(".slot").length == 0) $("#"+w).append("<div id='"+slotId+"' class='slot'></div>");
	for(var i = 0; i < cards.length; i++){
		var img = '<img data-index="'+i+'" src="http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid='+cards[i]["multiverseid"]+'&type=card">';
		$("#"+w+" #"+slotId+".slot").append(img);
		if(cards[i]["last"] && i < cards.length - 1){
			slotId++;
			$("#"+w).append("<div id='"+slotId+"' class='slot'></div>");
		}
	}
	// ADDING HOVER EVENTS TO HIGHLIGHT EACH CARD AFTER IT'S ON THE SCREEN
	$("#"+w+" img").on("mouseenter", function(e){
		$(this).css("z-index", 1);
	});
	$("#"+w+" img").on("mouseleave", function(e){
		$(this).css("z-index", 0);
	});
	$("#"+w+" img").on("click", function(e){
		var index = $(this).attr("data-index");
		switchCard(index, w);
	});
}

			function switchCard(i, w){
				switch(w){
					case "pool":
						var card = pool.splice(i, 1);
						deck.push(card[0]);
						printCards(pool, "pool");
						printCards(deck, "deck");
						break;
					case "deck":
						var card = deck.splice(i, 1);
						pool.push(card[0]);
						printCards(pool, "pool");
						printCards(deck, "deck");
						break;
				}
			}

			function colorSort(cards){
				cards.sort(function(a, b){
					return(a["colorInt"] - b["colorInt"] || a["cmc"] - b["cmc"] || a["nSymbols"] - b["nSymbols"] || (a["name"].toLowerCase()).localeCompare((b["name"].toLowerCase())));
				});
				for(var i = 0; i < cards.length; i++){
					if(cards[i]["last"]) delete cards[i]["last"];
					if(i != 0 && cards[i-1]["colorInt"] != cards[i]["colorInt"]) cards[i-1]["last"] = 1;
				}
				lastSortP = "color";
			}

			function raritySort(cards){
				cards.sort(function(a, b){
					return(a["rarInt"] - b["rarInt"] || (a["name"].toLowerCase()).localeCompare((b["name"].toLowerCase())));
				});
				for(var i = 0; i < cards.length; i++){
					if(cards[i]["last"]) delete cards[i]["last"];
					if(i != 0 && cards[i-1]["rarInt"] != cards[i]["rarInt"]) cards[i-1]["last"] = 1;
				}
				lastSortP = "rarity";
			}

			function cmcSort(cards){
				cards.sort(function(a, b){
					return(a["cmc"] - b["cmc"] || a["colorInt"] - b["colorInt"] || a["nSymbols"] - b["nSymbols"] || (a["name"].toLowerCase()).localeCompare((b["name"].toLowerCase())));
				});
				for(var i = 0; i < cards.length; i++){
					if(cards[i]["last"]) delete cards[i]["last"];
					if(i != 0 && cards[i-1]["cmc"] != cards[i]["cmc"]) cards[i-1]["last"] = 1;
				}
				lastSortP = "cmc";
			}

$(document).ready(function(){
			// ADDING THE BUTTON FUNCTIONS
			// SORTS FIRST BY COLOR, THEN CMC, THEN NUMBER OF SYMBOLS, AND FINALLY NAME
			$("#pool #colorSort").on("click", function(){
				colorSort(pool);
				printCards(pool, "pool");
			});

			$("#pool #raritySort").on("click", function(){
				raritySort(pool);
				printCards(pool, "pool");
			});

			$("#pool #cmcSort").on("click", function(){
				cmcSort(pool);
				printCards(pool, "pool");
			});

			$("#deck #colorSort").on("click", function(){
				colorSort(deck);
				printCards(deck, "deck");
			});

			$("#deck #raritySort").on("click", function(){
				raritySort(deck);
				printCards(deck, "deck");
			});

			$("#deck #cmcSort").on("click", function(){
				cmcSort(deck);
				printCards(deck, "deck");
			});

			$("#pool #pzoom").on("click", function(){
				var w = $(".slot").width();
				$(".slot").width(w + 10);
			});

			$("#pool #mzoom").on("click", function(){
				var w = $(".slot").width();
				$(".slot").width(w - 10);
			});


			$("#deck #save").on("click", function(e){
				// SORTS THE DECK BEFORE SENDING IT TO THE SERVER SO IT'S EASIER TO MAP AND REDUCE IT
				cmcSort(deck);
				$.post("engine.php", { deck: JSON.stringify(deck) }, null, "html")
					.done(function(data) {
						window.location.href = "engine.php?deck="+data;
					});
			});
});