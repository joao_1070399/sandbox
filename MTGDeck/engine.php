<?php
	function pr($var){
		echo "<pre>";
			print_r($var);
		echo "</pre>";
	}

	$allsets = json_decode(file_get_contents("./assets/AllSets.json"), true);

	function getRandomCard($exp, $rar){
		global $allsets;
		do {
			$rnum = rand(0, (count($allsets[$exp]["cards"]) - 1));
			$pos = strrpos($allsets[$exp]["cards"][$rnum]["number"], "b");
		} while($allsets[$exp]["cards"][$rnum]["rarity"] != $rar || $pos !== false);
		return $allsets[$exp]["cards"][$rnum];
	}

	function generateBooster($exp){
		$booster = Array();

		// GENERATES THE RARE OR MYTHIC
		$rnum = rand(1,8);
		if($rnum == 1) $card = getRandomCard($exp, "Mythic Rare");
			else $card = getRandomCard($exp, "Rare");
		array_push($booster, $card);

		// GENERATES THE UNCOMMONS
		for($i = 0; $i < 3; $i++){
			$card = getRandomCard($exp, "Uncommon");
			array_push($booster, $card);
		}

		// GENERATES THE COMMONS
		for($i = 0; $i < 10; $i++){
			$card = getRandomCard($exp, "Common");
			array_push($booster, $card);
		}

		return $booster;
	}

	function generatePool($boosters){
		$pool = Array();
		for($i = 0; $i < count($boosters); $i++){
			$booster = generateBooster($boosters[$i]);
			// THESE TWO FOR CYCLES RUN THROUGH EVERY CARD ON EACH OF THE 6 BOOSTERS
			// ADDS TO EVERY CARD SOME USEFUL PARAMETERS FOR SORTING: COLOR INTEGER AND NUMBER OF MANA SYMBOLS
			// FLATTENS THE ARRAY - TURNS 6 * 14 INTO 84
			// COLOR INTEGER
			for($j = 0; $j < count($booster); $j++){
				// RUNS THROUGH EACH COLOR OF THE CARD, AND ATTRIBUTES AN INTEGER FOR THE COLOR COMBINATION OF THE CARD
				$colorInt = 0;
				if(isset($booster[$j]["colors"]) && count($booster[$j]["colors"]) == 1){
					switch($booster[$j]["colors"][0]){
						case "White":
							$colorInt = 1;
							break;
						case "Blue":
							$colorInt = 2;
							break;
						case "Black":
							$colorInt = 3;
							break;
						case "Red":
							$colorInt = 4;
							break;
						case "Green":
							$colorInt = 5;
							break;
					}
				}
				if(isset($booster[$j]["colors"]) && count($booster[$j]["colors"]) > 1) $colorInt = 6;
				if(!isset($booster[$j]["colors"])){
					if(strrpos($booster[$j]["type"], "Land") !== false) $colorInt = 8;
						else $colorInt = 7;
				}
				$booster[$j]["colorInt"] = $colorInt;

				// DOES THE SAME BUT FOR RARITY (HIGHER RARITY = LOWER NUMBER)
				// RUNS THROUGH EACH COLOR OF THE CARD, AND ATTRIBUTES AN INTEGER FOR THE COLOR COMBINATION OF THE CARD
				if(isset($booster[$j]["rarity"])){
					switch($booster[$j]["rarity"]){
						case "Mythic Rare":
							$rarInt = 1;
							break;
						case "Rare":
							$rarInt = 2;
							break;
						case "Uncommon":
							$rarInt = 3;
							break;
						case "Common":
							$rarInt = 4;
							break;
						default:
							$rarInt = 5;
							break;
						}
						$booster[$j]["rarInt"] = $rarInt;
				} else $booster[$j]["rarInt"] = 5;

				// COUNTS THE NUMBER OF MANA SYMBOLS
				if(isset($booster[$j]["manaCost"])){
					$nSymbols = preg_split("/[0-9{}X]/", $booster[$j]["manaCost"], -1, PREG_SPLIT_NO_EMPTY);
					$booster[$j]["nSymbols"] = count($nSymbols);
				}

				// LASTLY, GIVES IT A "LAST" (hehe) ATTRIBUTE EVERY 14 CARDS, SO THE ENGINE KNOWS WHEN TO PLACE THE CARDS IN THE DIFFERENT COLUMNS
				if($j == count($booster) - 1) $booster[$j]["last"] = 1;

				array_push($pool, $booster[$j]);
			}
		}
		return $pool;
	}

	// WRITES THE ARRAY TO A TEXT FILE
	function writeDeck($deck){
		$deck = json_decode($deck, true);
		$final = Array();
		$fi = 0;
		$fname = uniqid().".txt";
		$file = fopen($fname,"w");

		// THIS REDUCES THE ARRAY (TURNS 1 ISLAND 1 ISLAND INTO 2 ISLAND)
		for($i = 0; $i < count($deck); $i++){
			if($i == 0){ $final[$fi]["qty"] = 1; $final[$fi]["card"] = $deck[$i]["name"]; }
				else{
					if($deck[$i]["name"] == $deck[$i-1]["name"]) $final[$fi]["qty"]++;
						else { $fi++; $final[$fi]["qty"] = 1; $final[$fi]["card"] = $deck[$i]["name"]; }
				}
		}

		for($i = 0; $i < count($final); $i++){
			fwrite($file, $final[$i]["qty"]." ".$final[$i]["card"]."\n");
		}

		echo $fname;
		fclose($file);
	}

	if(isset($_GET["deck"])){
		$file = $_GET["deck"];

		if (file_exists($file)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="deck_'.date("d-m-Y_G-i-s").'.txt"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			exit;
		}
	}

	if(isset($_POST["deck"])) writeDeck($_POST["deck"]);
		else echo json_encode(generatePool(["SOI", "SOI", "SOI", "SOI", "SOI", "SOI"]));
?>